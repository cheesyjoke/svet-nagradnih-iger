<?php

$time1 = microtime(true);

$user = "root";
$pass = "";
$sPot = "C:\\XAMPP\\htdocs\\Svet_Nagradnih_Iger\\slike\\";

try {
    $conn = new PDO('mysql: host=localhost:8080;dbname=projekti', $user, $pass);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sSQL = "SELECT * FROM projekti.nagradne_igre";
    foreach ($conn->query($sSQL) as $row) {
        
        $iID = $row["igraID"];
        $slika = $row["igra_slika"];
        $slika = imagecreatefromstring($slika);
        $sName = $row["igra_ime"] . "_" . $iID . ".png";
        if(!file_exists("./slike/" . $sName)){
            imagepng($slika, $sPot . $sName);
            echo "shranil sliko <br>";
        } else {
            echo "slika ze obstaja <br>";
        }
    }
    
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$time2 = microtime(true);
echo 'script execution time: ' . ($time2 - $time1);
?>