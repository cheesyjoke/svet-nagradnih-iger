#03.03.2018
CREATE TABLE `nagradne_igre` (
 `igraID` int(11) NOT NULL,
 `igra_ime` varchar(50) NOT NULL,
 `podjetjeID` int(11) NOT NULL,
 `kategorijaID` int(11) NOT NULL,
 `igra_slika` mediumblob NOT NULL,
 `opis` varchar(200) NOT NULL,
 PRIMARY KEY (`igraID`),
 UNIQUE KEY `UNIKAT` (`igra_ime`)
) ENGINE=InnoDB;